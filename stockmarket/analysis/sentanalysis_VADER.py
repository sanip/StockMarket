'''
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

Created on Sun Apr 22 20:57:10 2018
@author: aayushsharma
A python program that gives sentiment scores for news from a CSV file.
'''

import csv
from pprint import pprint
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from nltk.sentiment.vader import SentimentIntensityAnalyzer as SIA
# import seaborn as sns
# sns.set(style='darkgrid', context='talk', palette='Dark2')
news = []
date=[]
url=[]
vader_data={}
def vader_company(filename):
    with open(filename+"_news.csv") as f:
        reader = csv.DictReader(f)
        data = [r for r in reader]
        data.pop(0)  # remove header
        x = 0
        while x < len(data):

            news.append(data[x]['content'])
            date.append(data[x]['date'])
            url.append(data[x]['link'])
            x = x + 1
            # print(news)

    sia = SIA()
    results = []
    pol_score={}


    for line,dt,urls in zip(news,date,url):
        pol_score = sia.polarity_scores(line)
        # pol_score['news'] = line
        pol_score['url'] = urls
        pol_score['symbol'] = filename
        pol_score['date'] = dt
        results.append(pol_score)




    pprint(results,width=100)

    # x=0
    # while x < len(results):
    #     if results[x]['compound']<0:
    #         print(results[x])
    #     x=x+1


    # df = pd.DataFrame.from_records(results)
    # df.head()

    """We will consider posts with a compound value greater than 0.2 as positive and 
    less than -0.2 as negative. There's some testing and experimentation that goes with
     choosing these ranges, and there is a trade-off to be made here. If you choose a 
     higher value, you might get more compact results (less false positives and false 
     negatives), but the size of the results will decrease significantly."""
    #
    # df['label'] = 0
    # df.loc[df['compound'] > 0.05, 'label'] = 1
    # df.loc[df['compound']< -0.05, 'label'] = -1
    # # df.head()
    #
    # df2 = df[['news','label']]
    # df2.to_csv('news_labels.csv', mode='a', encoding='utf-8', index=False)

    # print("Positive News:\n")
    # pprint(list(df[df['label'] == 1].news)[:5], width=200)
    #
    # print("Negative News:\n")
    # pprint(list(df[df['label'] == -1].news)[:5], width=200)
    #
    # print(df.label.value_counts())
    # print(df.label.value_counts(normalize=True) *100 )

    with open("newssent.csv", 'a') as newsfile:
        writer = csv.DictWriter(newsfile, fieldnames=["compound", "date", "neg", "neu","pos","symbol","url"])
        writer.writeheader()
        writer.writerows(results)
        print("Writing to CSV " + str(filename))
    f.close()
#fig , ax = plt.subplots(figsize=(8,8))
#counts = df.label.value_counts(normalize=True)*100
#sns.barplot(x=counts.index, y=counts, ax=ax)
#ax.set_xticklabels(['Negative', 'Neutral', 'Positive'])
#ax.set_ylabel("Percentage")
#plt.show()

if __name__ == '__main__':
    vader_company("ADBL")
    vader_company("AHPC")
    vader_company("AKPL")
    vader_company("ALICL")
    vader_company("API")
