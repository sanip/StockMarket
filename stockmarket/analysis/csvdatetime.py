
'''
Convert dates in CSV to datetime in python
'''
import csv
from datetime import datetime
def datetimecol(filename):
    with open(filename) as f:
        reader = csv.reader(f, delimiter=',')
        line_count=0
        for row in reader:
            if line_count==0:
                line_count+=1
                continue
            else:
                print(datetime.strptime(row[0],'%B %d, %Y'))
                with open(filename, 'a') as fw:
                    fwriter = csv.writer(fw)
                    for ro in reader:
                        if line_count == 0:
                            line_count += 1
                            continue
                        else:
                            time = datetime.strptime(ro[0],'%B %d, %Y')
                            fwriter.writerow([time]+ro[1:])







if __name__ == '__main__':
    datetimecol("ADBL_news.csv")