import celery
from . import company_classifier
from . import MLR
from . import companylist
from . import csvdatetime
from . import main
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import psycopg2
import datetime
import csv
from pprint import pprint
from nltk.sentiment.vader import SentimentIntensityAnalyzer as SIA

@celery.task
def classifier():
    company_classifier.classifier()

@celery.task
def MLR():
    symbol = 'ADBL'
    conn = psycopg2.connect(host="localhost", database="stockmarket", user="aayushsharma",
                            password="")

    # Importing the dataset
    sql = """SELECT id,symbol_id,date,ltp,change,high,low,open,news_score,prediction
    FROM analysis_companydata
    WHERE symbol_id ='""" + symbol + "';"
    dataset = pd.read_sql_query(sql, conn)
    print(dataset.tail())
    # dataset.iloc[:,2:3]= np.reshape(dataset.iloc[:,2:3], (len(dataset.iloc[:,2:3]), 1))
    X = dataset.iloc[:, 3:-1].values
    y = dataset.iloc[:, 9].values

    #
    ## Encoding categorical data
    # from sklearn.preprocessing import LabelEncoder, OneHotEncoder
    # labelencoder = LabelEncoder()
    # X[:, 3] = labelencoder.fit_transform(X[:, 3])
    # onehotencoder = OneHotEncoder(categorical_features = [3])
    # X = onehotencoder.fit_transform(X).toarray()

    ## Avoiding the Dummy Variable Trap
    # X = X[:, 1:]
    #
    # Splitting the dataset into the Training set and Test set
    from sklearn.cross_validation import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

    # Feature Scaling
    """from sklearn.preprocessing import StandardScaler
    sc_X = StandardScaler()
    X_train = sc_X.fit_transform(X_train)
    X_test = sc_X.transform(X_test)
    sc_y = StandardScaler()
    y_train = sc_y.fit_transform(y_train)"""

    # Fitting Multiple Linear Regression to the Training set
    from sklearn.linear_model import LinearRegression
    regressor = LinearRegression()
    regressor.fit(X_train, y_train)

    # Predicting the Test set results
    y_pred = regressor.predict(X_test)
    print(y_pred)

@celery.task
def sentianalysis():
    news = []
    date = []
    url = []
    vader_data = {}

    def vader_company(filename):
        with open(filename + "_news.csv") as f:
            reader = csv.DictReader(f)
            data = [r for r in reader]
            data.pop(0)  # remove header
            x = 0
            while x < len(data):
                news.append(data[x]['content'])
                date.append(data[x]['date'])
                url.append(data[x]['link'])
                x = x + 1
                # print(news)

        sia = SIA()
        results = []
        pol_score = {}

        for line, dt, urls in zip(news, date, url):
            pol_score = sia.polarity_scores(line)
            # pol_score['news'] = line
            pol_score['url'] = urls
            pol_score['symbol'] = filename
            pol_score['date'] = dt
            results.append(pol_score)

        pprint(results, width=100)

        # x=0
        # while x < len(results):
        #     if results[x]['compound']<0:
        #         print(results[x])
        #     x=x+1

        # df = pd.DataFrame.from_records(results)
        # df.head()

        """We will consider posts with a compound value greater than 0.2 as positive and 
        less than -0.2 as negative. There's some testing and experimentation that goes with
         choosing these ranges, and there is a trade-off to be made here. If you choose a 
         higher value, you might get more compact results (less false positives and false 
         negatives), but the size of the results will decrease significantly."""
        #
        # df['label'] = 0
        # df.loc[df['compound'] > 0.05, 'label'] = 1
        # df.loc[df['compound']< -0.05, 'label'] = -1
        # # df.head()
        #
        # df2 = df[['news','label']]
        # df2.to_csv('news_labels.csv', mode='a', encoding='utf-8', index=False)

        # print("Positive News:\n")
        # pprint(list(df[df['label'] == 1].news)[:5], width=200)
        #
        # print("Negative News:\n")
        # pprint(list(df[df['label'] == -1].news)[:5], width=200)
        #
        # print(df.label.value_counts())
        # print(df.label.value_counts(normalize=True) *100 )

        with open("newssent.csv", 'a') as newsfile:
            writer = csv.DictWriter(newsfile, fieldnames=["compound", "date", "neg", "neu", "pos", "symbol", "url"])
            writer.writeheader()
            writer.writerows(results)
            print("Writing to CSV " + str(filename))
        f.close()

    # fig , ax = plt.subplots(figsize=(8,8))
    # counts = df.label.value_counts(normalize=True)*100
    # sns.barplot(x=counts.index, y=counts, ax=ax)
    # ax.set_xticklabels(['Negative', 'Neutral', 'Positive'])
    # ax.set_ylabel("Percentage")
    # plt.show()

    if __name__ == '__main__':
        vader_company("ADBL")
        vader_company("AHPC")
        vader_company("AKPL")
        vader_company("ALICL")
        vader_company("API")





