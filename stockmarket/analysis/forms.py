from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django.core.validators import RegexValidator

from .models import *

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length = 30, required = True, help_text = "Required.")
    last_name = forms.CharField(max_length = 30, required = True, help_text = "Required.")
    email = forms.EmailField(max_length=254, required = True, help_text='Required. Inform a valid email address.')
    address = forms.CharField(max_length=100, required = True, help_text='Required')
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    contact = models.CharField(validators=[phone_regex], max_length=17, blank=True)

    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name', 'email', 'address', 'contact', 'username', 'password1', 'password2')


class EditProfileForm(forms.ModelForm):
    username = forms.CharField(max_length=50, required=True)
    first_name = forms.CharField(max_length = 30, required = True, help_text = "Required.")
    last_name = forms.CharField(max_length = 30, required = True, help_text = "Required.")
    email = forms.EmailField(max_length=254, required = True, help_text='Required. Inform a valid email address.')
    address = forms.CharField(max_length=100, required = True, help_text='Required')
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    contact = models.CharField(validators=[phone_regex], max_length=17, blank=True)

    class Meta:
        model = Users
        fields = ('username', 'first_name', 'last_name', 'email', 'address', 'contact')

class LoginForm(forms.Form):
    username = forms.CharField(max_length=50, required = True)
    password = forms.CharField(widget=forms.PasswordInput())

class UnknownForm(forms.Form):
    choices = forms.MultipleChoiceField(
        widget=forms.CheckboxSelectMultiple
    )

class EmailForm(forms.Form):
    to = forms.EmailField(max_length=100, required=True, label='To')
    subject = forms.CharField(max_length=250, required=True, label='Subject')
    message = forms.CharField(max_length=500, required=True, label='Message')



