# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import psycopg2
import datetime

symbol='ADBL'
conn = psycopg2.connect(host="localhost",database="stockmarket", user="aayushsharma",
                        password="")

#Importing the dataset
sql="""SELECT id,symbol_id,date,ltp,change,high,low,open,news_score,prediction
FROM analysis_companydata
WHERE symbol_id ='"""+symbol+"';"
dataset = pd.read_sql_query(sql,conn)
print(dataset.tail())
#dataset.iloc[:,2:3]= np.reshape(dataset.iloc[:,2:3], (len(dataset.iloc[:,2:3]), 1))
X = dataset.iloc[:, 3:-1].values
y = dataset.iloc[:, 9].values

#
## Encoding categorical data
#from sklearn.preprocessing import LabelEncoder, OneHotEncoder
#labelencoder = LabelEncoder()
#X[:, 3] = labelencoder.fit_transform(X[:, 3])
#onehotencoder = OneHotEncoder(categorical_features = [3])
#X = onehotencoder.fit_transform(X).toarray()

## Avoiding the Dummy Variable Trap
#X = X[:, 1:]
#
# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Feature Scaling
"""from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train)"""

# Fitting Multiple Linear Regression to the Training set
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(X_train, y_train)

# Predicting the Test set results
y_pred = regressor.predict(X_test)
print(y_pred)