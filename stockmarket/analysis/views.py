import sys

from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from django.template import context
from selenium import webdriver
from bs4 import BeautifulSoup as soup
from selenium.webdriver.chrome.options import Options
from pyvirtualdisplay import Display
from django.core.mail import send_mail


from .forms import *

# Create your views here.
def index_refresh(request):
    display = Display(visible=0, size=(800, 600))
    display.start()
    options = Options()
    options.add_argument("--no-sandbox")
    driver = webdriver.Chrome('/usr/bin/chromedriver', chrome_options=options)
    driver.get("http://nepalstock.com.np")
    html = driver.page_source
    page = soup(html, "html.parser")
    marquee = page.find('marquee')

    #if internet connection is on return live data
    if marquee:
        string = "".join(str(x) for x in marquee)
        if marquee:
            return JsonResponse({
                'html': string
            })
        else:
            return JsonResponse(0)

    #if no internet connection, display the prepared data
    else:
        marquee = '<b>ADBL 332.00 ( 360 ) ( <span class="red"> -1 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    AHPC 134.00 ( 19 ) ( <span class="green"> 2 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/increase.gif" width="8"/>    AMFI 835.00 ( 60 ) ( <span class="red"> -4 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    BFC 99.00 ( 300 ) ( <span class="red"> -1 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    BOKL 279.00 ( 1000 ) ( <span class="green"> 5 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/increase.gif" width="8"/>    BPCL 430.00 ( 70 ) ( <span class="red"> -3 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    CBL 150.00 ( 55 ) ( <span class="green"> 2 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/increase.gif" width="8"/>    CCBL 165.00 ( 642 ) ( <span class="red"> -2 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    CFCL 113.00 ( 10 ) ( <span class="red"> -2 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    CIT 2,400.00 ( 12 ) ( <span class=""> 0 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/nil.gif" width="8"/>    EBL 654.00 ( 153 ) ( <span class=""> 0 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/nil.gif" width="8"/>    EIC 1,220.00 ( 11 ) ( <span class="red"> -12 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    FMDBL 402.00 ( 483 ) ( <span class="red"> -4 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    GBIME 290.00 ( 3002 ) ( <span class="red"> -4 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    GBLBS 560.00 ( 190 ) ( <span class="red"> -10 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    GLICL 800.00 ( 2 ) ( <span class="red"> -14 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    HIDCL 160.00 ( 56 ) ( <span class=""> 0 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/nil.gif" width="8"/>    HPPL 246.00 ( 10 ) ( <span class="red"> -5 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    JBBL 142.00 ( 8 ) ( <span class="red"> -2 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    JBNL 152.00 ( 2264 ) ( <span class="red"> -1 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    JFL 193.00 ( 520 ) ( <span class="green"> 3 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/increase.gif" width="8"/>    KBBL 232.00 ( 751 ) ( <span class="green"> 3 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/increase.gif" width="8"/>    KRBL 108.00 ( 44 ) ( <span class="green"> 2 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/increase.gif" width="8"/>    KSBBL 140.00 ( 3 ) ( <span class="red"> -2 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    LBBL 147.00 ( 2561 ) ( <span class=""> 0 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/nil.gif" width="8"/>    LBL 230.00 ( 1157 ) ( <span class="red"> -2 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    MEGA 163.00 ( 124 ) ( <span class=""> 0 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/nil.gif" width="8"/>    MLBL 187.00 ( 100 ) ( <span class="red"> -6 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    MNBBL 380.00 ( 150 ) ( <span class="green"> 3 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/increase.gif" width="8"/>    NABIL 935.00 ( 50 ) ( <span class="red"> -5 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    NBB 200.00 ( 202 ) ( <span class="red"> -1 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    NBL 285.00 ( 429 ) ( <span class="red"> -1 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    NCCB 175.00 ( 7248 ) ( <span class="red"> -3 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    NGPL 157.00 ( 99 ) ( <span class="red"> -6 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    NHPC 77.00 ( 1000 ) ( <span class="red"> -1 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    NIB 626.00 ( 963 ) ( <span class="red"> -9 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    NICA 312.00 ( 33 ) ( <span class="red"> -2 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    NIL 504.00 ( 835 ) ( <span class="red"> -3 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    NLIC 919.00 ( 1284 ) ( <span class="red"> -16 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    NMB 318.00 ( 1750 ) ( <span class="red"> -5 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    NTC 742.00 ( 10 ) ( <span class="green"> 9 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/increase.gif" width="8"/>    PIC 1,040.00 ( 77 ) ( <span class="red"> -13 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    PLIC 500.00 ( 519 ) ( <span class="red"> -2 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    PRIN 476.00 ( 330 ) ( <span class="red"> -9 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    PRVU 180.00 ( 7 ) ( <span class="red"> -3 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    RADHI 217.00 ( 10 ) ( <span class="red"> -4 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    RMDC 640.00 ( 51 ) ( <span class="green"> 6 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/increase.gif" width="8"/>    RRHP 149.00 ( 420 ) ( <span class="green"> 2 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/increase.gif" width="8"/>    SAEF 9.00 ( 3200 ) ( <span class=""> 0 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/nil.gif" width="8"/>    SANIMA 312.00 ( 747 ) ( <span class="red"> -2 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    SBL 308.00 ( 529 ) ( <span class="red"> -4 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    SCB 724.00 ( 306 ) ( <span class="red"> -1 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    SHL 248.00 ( 200 ) ( <span class="green"> 7 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/increase.gif" width="8"/>    SHPC 300.00 ( 1000 ) ( <span class="red"> -6 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    SIL 666.00 ( 54 ) ( <span class="red"> -2 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    SMB 800.00 ( 10 ) ( <span class="red"> -12 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/>    SPDL 107.00 ( 25 ) ( <span class="red"> -3 </span>) <img align="middle" alt="" border="0" src="http://nepalstock.com.np/./images/decrease.gif" width="8"/></b>'
        string = "".join(str(x) for x in marquee)
        return JsonResponse({
            'html': string
        })

def index(request):
    companydata = Companydata.objects.all().filter(date='2018-01-14').values('symbol', 'ltp', 'change', 'prediction')
    burning = []
    highstocks = Companydata.objects.filter(date='2018-01-07').values('symbol', 'change').order_by('change')[:2]
    lowstocks = Companydata.objects.filter(date='2018-01-07').values('symbol', 'change').order_by('-change')[:2]
    burning.append(highstocks)
    burning.append(lowstocks)
    news = News.objects.filter(date='2018-04-04')
    for item in news:
        newsSent = Newssentiment.objects.get(url=item.url)
        item.score = newsSent.compound_score
    if request.method == 'POST':
        form = EmailForm(request.POST)
        to_mail = []
        if form.is_valid:
            to_mail.append(request.POST['to'])
            subject = request.POST['subject']
            message = request.POST['message']
            if request.user.is_authenticated:
                mail = request.user.email
                user = Users.objects.get(email=mail)
                from_mail = user.email
                send_mail(subject, message, from_mail, to_mail, fail_silently=False)

            else:
                messages.error = (request, 'You must be logged in to send email')
        return HttpResponseRedirect('/')

    else:
        form = EmailForm()
    return render(request, 'index.html', {'form': form, 'company':companydata, 'news':news, 'burning':burning})


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/user_login')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form':form})

@login_required
def user(request, username):
    return render(request, 'index.html')

def user_login(request):
    if request.method == 'POST':
        user = authenticate(username=request.POST['username'], password = request.POST['password'])
        if user:
            if user.is_active:
                login(request, user)
                username=request.POST['username']
                return redirect('/')
        else:
            messages.error(request, 'Either your username or password  is incorrect')

    form = LoginForm()
    return render(request, 'login.html', {'form': form})

@login_required
def user_logout(request):
    logout(request)
    return redirect('/')

@login_required
def user_edit(request, username):
    if request.method == 'POST':
        user = Users.objects.get(username=username)
        form = EditProfileForm(request.POST or None, instance=user)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    else:
        user = Users.objects.get(username=username)
        form = EditProfileForm(instance=user)

    return render(request, 'editprofile.html', {'form': form})

@login_required
def user_setting(request, username):
    company = Companydata.objects.all().values('symbol').distinct()
    user = Users.objects.get(username=username)
    if request.method == 'POST':
        form = UnknownForm(request.POST)
        if 'submit' in request.POST:
            for choice in request.POST.getlist('choices'):
                symbol = Company.objects.get(symbol=choice)
                Userstock.objects.create(userid_id=user.id, symbol_id=symbol.symbol)
            return HttpResponseRedirect('/')
        else:
            messages.info(request, 'form not valid')
    else:
        form = UnknownForm()
    return render(request, 'user.html', {'company':company, 'form':form})

def chart_data(request):
    dataset = Companydata.objects.all().values('date', 'ltp', 'symbol')
    symbol = list()
    ltp = list()
    date = list()
    for item in dataset:
        symbol.append(item['symbol'])
        ltp.append(item['ltp'])
        date.append(item['date'])

    return JsonResponse({
        'symbol':symbol,
        'ltp':ltp,
        'date':date
    })

@login_required
def email(request):
    mail = request.user.email
    user = Users.objects.get(email=mail)

    if request.method == 'POST':
        form = EmailForm(request.POST)
        if form.is_valid:
            from_mail = user.email
            to_mail = request.POST['to']
            subject = request.POST['subject']
            message = request.POST['message']
            send_mail(subject, message, from_mail, to_mail, fail_silently=False)

            return HttpResponseRedirect('/')
    else:
        form = EmailForm()

    return render(request, 'index.html', {'form':form})

def search(request):
    if request.method == 'GET':
        company = set()
        company.add(request.GET.get('search'))
        try:
            news = News.objects.filter(newsof=company)  # filter returns a list
        except News.DoesNotExist:
            news = None
        return render(request, "search.html", {"news": news, "newsof":company})
    else:
        return render(request, "search.html", {})
