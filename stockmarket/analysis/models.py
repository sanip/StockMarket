# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.contrib.auth.models import AbstractUser
from django.db import models


class Company(models.Model):
    symbol = models.CharField(primary_key=True, max_length=50)
    name = models.CharField(max_length=255)
    category = models.CharField(max_length=100)


class Companydata(models.Model):
    date = models.DateField()
    ltp = models.FloatField(blank=True, null=True)
    change = models.FloatField(blank=True, null=True)
    high = models.FloatField(blank=True, null=True)
    low = models.FloatField(blank=True, null=True)
    open = models.FloatField(blank=True, null=True)
    quantity = models.FloatField(blank=True, null=True)
    turnover = models.FloatField(blank=True, null=True)
    symbol = models.ForeignKey(Company, models.DO_NOTHING)
    prediction = models.FloatField(blank=True, null=True)
    news_score = models.FloatField(blank=True, null=True)


class News(models.Model):
    date = models.DateField()
    url = models.CharField(primary_key=True, max_length=256)
    headline = models.TextField()
    content = models.TextField()
    newsof = models.TextField()  # This field type is a guess.


class Newssentiment(models.Model):
    dates = models.DateField()
    compound_score = models.FloatField()
    positive_score = models.FloatField()
    negative_score = models.FloatField()
    neutral_score = models.FloatField()
    news_symbol = models.ForeignKey(Company, models.DO_NOTHING, db_column='news_symbol')
    url = models.ForeignKey(News, models.DO_NOTHING, primary_key=True)


class Users(AbstractUser):
    address = models.CharField(max_length=100)
    contact = models.CharField(max_length=100)

class Userstock(models.Model):
    symbol = models.ForeignKey(Company, models.DO_NOTHING)
    userid = models.ForeignKey(Users, models.DO_NOTHING)
