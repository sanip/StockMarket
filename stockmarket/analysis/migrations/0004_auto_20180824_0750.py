# Generated by Django 2.0.7 on 2018-08-24 07:50

from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
        ('analysis', '0003_auto_20180823_0434'),
    ]

    operations = [
        migrations.CreateModel(
            name='AnalysisUsers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('password', models.CharField(max_length=128)),
                ('last_login', models.DateTimeField(blank=True, null=True)),
                ('is_superuser', models.BooleanField()),
                ('username_field', models.CharField(max_length=150, unique=True)),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=150)),
                ('email', models.CharField(max_length=254)),
                ('is_staff', models.BooleanField()),
                ('is_active', models.BooleanField()),
                ('date_joined', models.DateTimeField()),
                ('address', models.CharField(max_length=100)),
                ('contact', models.CharField(max_length=100)),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'db_table': 'analysis_Users',
                'managed': True,
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='AnalysisCompany',
            fields=[
                ('symbol', models.CharField(max_length=50, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('category', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'analysis_company',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='AnalysisCompanydata',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('ltp', models.FloatField(blank=True, null=True)),
                ('change', models.FloatField(blank=True, null=True)),
                ('high', models.FloatField(blank=True, null=True)),
                ('low', models.FloatField(blank=True, null=True)),
                ('open', models.FloatField(blank=True, null=True)),
                ('quantity', models.FloatField(blank=True, null=True)),
                ('turnover', models.FloatField(blank=True, null=True)),
                ('prediction', models.FloatField(blank=True, null=True)),
                ('news_score', models.FloatField(blank=True, null=True)),
                ('symbol', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='analysis.AnalysisCompany')),
            ],
            options={
                'db_table': 'analysis_companydata',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='AnalysisNews',
            fields=[
                ('date', models.DateField()),
                ('url', models.CharField(max_length=256, primary_key=True, serialize=False)),
                ('headline', models.TextField()),
                ('content', models.TextField()),
                ('newsof', models.TextField()),
            ],
            options={
                'db_table': 'analysis_news',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='AnalysisNewssentiment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dates', models.DateField()),
                ('compound_score', models.FloatField()),
                ('positive_score', models.FloatField()),
                ('negative_score', models.FloatField()),
                ('neutral_score', models.FloatField()),
                ('news_symbol', models.ForeignKey(db_column='news_symbol', on_delete=django.db.models.deletion.DO_NOTHING, to='analysis.AnalysisCompany')),
                ('url', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='analysis.AnalysisNews')),
            ],
            options={
                'db_table': 'analysis_newssentiment',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='AnalysisUserstock',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('symbol', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='analysis.AnalysisCompany')),
                ('userid', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'analysis_Userstock',
                'managed': True,
            },
        ),
        migrations.RemoveField(
            model_name='companydata',
            name='symbol',
        ),
        migrations.RemoveField(
            model_name='newssentiment',
            name='symbol',
        ),
        migrations.RemoveField(
            model_name='newssentiment',
            name='url',
        ),
        migrations.RemoveField(
            model_name='users',
            name='groups',
        ),
        migrations.RemoveField(
            model_name='users',
            name='user_permissions',
        ),
        migrations.RemoveField(
            model_name='userstock',
            name='symbol',
        ),
        migrations.RemoveField(
            model_name='userstock',
            name='userid',
        ),
        migrations.DeleteModel(
            name='Company',
        ),
        migrations.DeleteModel(
            name='CompanyData',
        ),
        migrations.DeleteModel(
            name='News',
        ),
        migrations.DeleteModel(
            name='NewsSentiment',
        ),
        migrations.DeleteModel(
            name='Users',
        ),
        migrations.DeleteModel(
            name='Userstock',
        ),
    ]
