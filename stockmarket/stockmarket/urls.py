"""stockmarket URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path

from analysis import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index , name = "index"),
    path('data/', views.index_refresh, name="refresh"),
    path('chart_data/', views.chart_data, name="chart_data"),
    path('signup/', views.signup, name='signup'),
    path('user_login/', views.user_login, name='user_login'),
    path('search/', views.search, name = "search"),
    path('user_logout/', views.user_logout, name="user_logout"),
    path('<slug:username>/', views.user, name="user"),
    path('<slug:username>/edit/', views.user_edit, name="user_edit"),
    path('<slug:username>/setting/', views.user_setting, name="user_setting"),

]
